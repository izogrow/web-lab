create table Track(
    name text,
    author text,
    size double precision NOT NULL,
    duration integer NOT NULL,
    description text,
    PRIMARY KEY (name, author)
);

create table Client(
    client_id serial PRIMARY KEY,
    username text UNIQUE,
    password text NOT NULL,
    name text
);

create table Tracklist(
    tracklist_id serial PRIMARY KEY,
    title text DEFAULT 'default',
    client_id integer REFERENCES Client(client_id) ON DELETE CASCADE,
    UNIQUE (title, client_id)
);

create table Tracklist_item(
    id serial PRIMARY KEY,
    tracklist_id integer REFERENCES Tracklist ON DELETE CASCADE,
    name text,
    author text,
    FOREIGN KEY (name, author) REFERENCES Track(name, author)
        ON DELETE CASCADE
);







