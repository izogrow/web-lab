insert into Client(username, password) values ('chuvashev', 1234);
insert into Client(username, password) values ('ivanov', 1234);
insert into Client(username, password) values ('petrov', 1234);
insert into Client(username, password) values ('sydorov', 1234);
insert into Client(username, password) values ('kuznetsov', 1234);

insert into Track(name, author, size, duration) values ('If We Newer', 'Aiobahn', 8.2, 200000);
insert into Track(name, author, size, duration) values ('I Cant Wait', 'Droeloe', 6, 143000);
insert into Track(name, author, size, duration) values ('Virtual Friends', 'Droeloe', 6.2, 150000);
insert into Track(name, author, size, duration) values ('Grind & Hustle', 'Droeloe', 7, 152000);
insert into Track(name, author, size, duration) values ('Push Through', 'Droeloe', 5, 180000);
insert into Track(name, author, size, duration) values ('Birch View', 'Glacier', 14, 342000);
insert into Track(name, author, size, duration) values ('Once Again', 'Tristam', 8, 202000);
insert into Track(name, author, size, duration) values ('Los Angeles', 'Robotaki', 9, 190000);
insert into Track(name, author, size, duration) values ('Wishes', 'Grant', 8, 200000);
insert into Track(name, author, size, duration) values ('Are We Still Young', 'Grant', 10, 220000);

insert into Tracklist(client_id) values (2);
insert into Tracklist(client_id, title) values (2, 'electronic');
insert into Tracklist(client_id) values (3);
insert into Tracklist(client_id, title) values (3, 'dope music');
insert into Tracklist(client_id) values (4);
insert into Tracklist(client_id, title) values (4, 'electro');
insert into Tracklist(client_id) values (5);
insert into Tracklist(client_id, title) values (5, 'my first tracklist');
insert into Tracklist(client_id) values (6);
insert into Tracklist(client_id, title) values (6, 'truely music!');

insert into Tracklist_item(tracklist_id, name, author) values (1, 'If We Newer', 'Aiobahn');
insert into Tracklist_item(tracklist_id, name, author) values (2, 'If We Newer', 'Aiobahn');
insert into Tracklist_item(tracklist_id, name, author) values (5, 'If We Newer', 'Aiobahn');
insert into Tracklist_item(tracklist_id, name, author) values (1, 'Virtual Friends', 'Droeloe');
insert into Tracklist_item(tracklist_id, name, author) values (3, 'Virtual Friends', 'Droeloe');
insert into Tracklist_item(tracklist_id, name, author) values (4, 'Virtual Friends', 'Droeloe');
insert into Tracklist_item(tracklist_id, name, author) values (1, 'I Cant Wait', 'Droeloe');
insert into Tracklist_item(tracklist_id, name, author) values (2, 'Are We Still Young', 'Grant');
insert into Tracklist_item(tracklist_id, name, author) values (6, 'Are We Still Young', 'Grant');
insert into Tracklist_item(tracklist_id, name, author) values (9, 'Are We Still Young', 'Grant');
insert into Tracklist_item(tracklist_id, name, author) values (9, 'Grind & Hustle', 'Droeloe');
insert into Tracklist_item(tracklist_id, name, author) values (7, 'Grind & Hustle', 'Droeloe');
insert into Tracklist_item(tracklist_id, name, author) values (8, 'Grind & Hustle', 'Droeloe');
insert into Tracklist_item(tracklist_id, name, author) values (10, 'Grind & Hustle', 'Droeloe');





