package com.chuvashev;

import com.chuvashev.dao.ClientDAO;
import com.chuvashev.dao.TrackDAO;
import com.chuvashev.dao.TracklistDAO;
import com.chuvashev.dao.TracklistItemDAO;
import com.chuvashev.dao.impl.ClientDAOImpl;
import com.chuvashev.dao.impl.TrackDAOImpl;
import com.chuvashev.dao.impl.TracklistDAOImpl;
import com.chuvashev.dao.impl.TracklistItemDAOImpl;
import com.chuvashev.entities.Client;
import com.chuvashev.entities.Track;

import java.sql.SQLException;

public class Main {
    public static void main(String[] args) throws SQLException {
        ClientDAO clientDAO = new ClientDAOImpl();
        System.out.println(clientDAO.getAll());
        TracklistDAO tracklistDAO = new TracklistDAOImpl();
        System.out.println(tracklistDAO.getAll());
        TracklistItemDAO tracklistItemDAO = new TracklistItemDAOImpl();
        System.out.println(tracklistItemDAO.get(2));
    }
}
