package com.chuvashev.dao;

import com.chuvashev.entities.Track;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public interface TrackDAO {
    String SQL_INSERT = "insert into " + Track.TABLE_NAME + "( " + Track.AUTHOR_COLUMN + ", " +
            Track.NAME_COLUMN + ", " + Track.SIZE_COLUMN + ", " + Track.DURATION_COLUMN + ") values (?, ?, ?, ?)";
    String SQL_DELETE = "delete from " + Track.TABLE_NAME + " where " + Track.NAME_COLUMN + " = ? and " +
            Track.AUTHOR_COLUMN + " = ?";

    void save(Track track) throws SQLException;
    void delete(Track track) throws SQLException;
}
