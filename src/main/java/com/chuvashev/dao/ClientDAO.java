package com.chuvashev.dao;

import com.chuvashev.entities.Client;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public interface ClientDAO{
    String SQL_INSERT = "insert into " + Client.TABLE_NAME + "( " + Client.USERNAME_COLUMN + ", " +
            Client.PASSWORD_COLUMN + ") values (?, ?)";
    String SQL_DELETE_BY_ID = "delete from " + Client.TABLE_NAME + " where " + Client.ID_COLUMN + " = ?";
    String SQL_SELECT = "select " + Client.ID_COLUMN + " from " + Client.TABLE_NAME + " where " + Client.USERNAME_COLUMN
            + " = ? and " + Client.PASSWORD_COLUMN + " = ?";
    String SQL_SELECT_ALL = "select " + Client.ID_COLUMN + ", " + Client.USERNAME_COLUMN + ", " + Client.PASSWORD_COLUMN
            + ", " + Client.NAME_COLUMN + " from " + Client.TABLE_NAME;
    String SQL_SELECT_BY_ID = "select " + Client.ID_COLUMN + " from " + Client.TABLE_NAME + " where " + Client.ID_COLUMN
            + " = ?";
    String SQL_UPDATE = "update " + Client.TABLE_NAME + " set " + Client.USERNAME_COLUMN + " = ? where " +
            Client.USERNAME_COLUMN + " = ?";

    Optional<Client> get(long id) throws SQLException;

    List<Client> getAll() throws SQLException;

    void save(Client client) throws SQLException;

    void update(String oldUsername, String newUsername) throws SQLException;

    void delete(Client client) throws SQLException;
}
