package com.chuvashev.dao;

import com.chuvashev.entities.TracklistItem;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public interface TracklistItemDAO{
    String SQL_INSERT = "insert into " + TracklistItem.TABLE_NAME + "( " + TracklistItem.NAME_COLUMN + ", " +
            TracklistItem.AUTHOR_COLUMN + ", " + TracklistItem.TRACKLIST_ID_COLUMN + ") values (?, ?, ?)";
    String SQL_DELETE_BY_ID = "delete from " + TracklistItem.TABLE_NAME + " where " + TracklistItem.ID_COLUMN + " = ?";
    String SQL_SELECT_BY_ID = "select " + TracklistItem.NAME_COLUMN + ", " + TracklistItem.AUTHOR_COLUMN + ", " +
            TracklistItem.ID_COLUMN + " from " + TracklistItem.TABLE_NAME + " where " +
            TracklistItem.TRACKLIST_ID_COLUMN + " = ?";

    Optional<TracklistItem> get(long tracklistId) throws SQLException;
  //  List<TracklistItem> getAll() throws SQLException;
    void save(TracklistItem tracklistItem) throws SQLException;
    void delete(long id) throws SQLException;
}
