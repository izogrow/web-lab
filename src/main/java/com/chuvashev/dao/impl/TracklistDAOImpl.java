package com.chuvashev.dao.impl;

import com.chuvashev.Database;
import com.chuvashev.dao.TracklistDAO;
import com.chuvashev.entities.Tracklist;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class TracklistDAOImpl implements TracklistDAO {
    @Override
    public Optional<Tracklist> get(long id) throws SQLException {
        try (Connection connection = Database.getConnection()) {
            Tracklist tracklist = null;
            PreparedStatement statement = connection.prepareStatement(SQL_SELECT_BY_ID);
            statement.setLong(1, id);

            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                tracklist = new Tracklist();
                tracklist.setId(resultSet.getLong(Tracklist.ID_COLUMN));
                tracklist.setClientId(resultSet.getLong(Tracklist.CLIENT_ID_COLUMN));
                tracklist.setTitle(resultSet.getString(Tracklist.TITLE_COLUMN));
                return Optional.of(tracklist);
            }
            return Optional.empty();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
            throw throwables;
        }
    }

    @Override
    public List<Tracklist> getAll(long clientId) throws SQLException {
        try (Connection connection = Database.getConnection()) {
            List<Tracklist> tracklists = new LinkedList<>();
            PreparedStatement statement = connection.prepareStatement(SQL_SELECT_BY_CLIENT_ID);
            statement.setLong(1, clientId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Tracklist tracklist = new Tracklist();
                tracklist.setTitle(resultSet.getString(Tracklist.TITLE_COLUMN));
                tracklist.setId(resultSet.getLong(Tracklist.ID_COLUMN));
                tracklist.setClientId(resultSet.getLong(Tracklist.CLIENT_ID_COLUMN));
                tracklists.add(tracklist);
            }
            return tracklists;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            throw throwables;
        }
    }

    @Override
    public List<Tracklist> getAll() throws SQLException {
        try (Connection connection = Database.getConnection()) {
            List<Tracklist> tracklists = new LinkedList<>();
            PreparedStatement statement = connection.prepareStatement(SQL_SELECT_ALL);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Tracklist tracklist = new Tracklist();
                tracklist.setTitle(resultSet.getString(Tracklist.TITLE_COLUMN));
                tracklist.setId(resultSet.getLong(Tracklist.ID_COLUMN));
                tracklist.setClientId(resultSet.getLong(Tracklist.CLIENT_ID_COLUMN));
                tracklists.add(tracklist);
            }
            return tracklists;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
            throw throwables;
        }
    }

    @Override
    public void save(Tracklist tracklist) throws SQLException {
        try (Connection connection = Database.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SQL_INSERT);
            statement.setString(1, tracklist.getTitle());
            statement.setLong(2, tracklist.getClientId());
            statement.execute();

            ResultSet keys = statement.getGeneratedKeys();
            if (keys.next()) {
                tracklist.setId(keys.getLong(1));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            throw throwables;
        }
    }

    @Override
    public void delete(long id) throws SQLException {
        try (Connection connection = Database.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SQL_DELETE_BY_ID);
            statement.setLong(1, id);
            statement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            throw throwables;
        }
    }
}
