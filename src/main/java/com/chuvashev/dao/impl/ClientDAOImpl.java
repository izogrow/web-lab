package com.chuvashev.dao.impl;

import com.chuvashev.Database;
import com.chuvashev.dao.ClientDAO;
import com.chuvashev.entities.Client;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ClientDAOImpl implements ClientDAO {

    public static void main(String[] args) throws SQLException {
        ClientDAO clientDAO = new ClientDAOImpl();
        System.out.println(clientDAO.getAll());
       // clientDAO.save(new Client(0L, null, "izogrow", "1234"));
        //clientDAO.getAll();
    }

    @Override
    public Optional<Client> get(long id) throws SQLException {
        try (Connection connection = Database.getConnection()) {
            Client client = null;
            PreparedStatement statement = connection.prepareStatement(SQL_SELECT_BY_ID);
            statement.setLong(1, id);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                client = new Client();
                client.setId(id);
                client.setName(rs.getString(Client.NAME_COLUMN));
                client.setUsername(rs.getString(Client.USERNAME_COLUMN));
                client.setPassword(rs.getString(Client.PASSWORD_COLUMN));
                return Optional.of(client);
            }
            rs.close();
            statement.close();
            connection.close();
            return Optional.empty();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            throw throwables;
        }
    }

    @Override
    public List<Client> getAll() throws SQLException {
        try (Connection connection = Database.getConnection()) {
            List<Client> clients = new ArrayList<>();
            PreparedStatement statement = connection.prepareStatement(SQL_SELECT_ALL);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Client client = new Client();
                client.setId(rs.getLong(Client.ID_COLUMN));
                client.setName(rs.getString(Client.NAME_COLUMN));
                client.setUsername(rs.getString(Client.USERNAME_COLUMN));
                client.setPassword(rs.getString(Client.PASSWORD_COLUMN));
                clients.add(client);
            }
            return clients;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            throw throwables;
        }
    }

    @Override
    public void save(Client client) throws SQLException {   // добовляем только юзернейм и пароль
        try (Connection connection = Database.getConnection()){
           PreparedStatement statement = connection.prepareStatement(SQL_INSERT);
           statement.setString(1, client.getUsername());
           statement.setString(2, client.getPassword());
           statement.execute();

           ResultSet generatedKeys = statement.getGeneratedKeys();
           if (generatedKeys.next()) {
               client.setId(generatedKeys.getLong(1));
           }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            throw throwables;
        }
    }

    @Override
    public void update(String oldUsername, String newUsername) throws SQLException {
        try (Connection connection = Database.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SQL_UPDATE);
            statement.setString(1, newUsername);
            statement.setString(2, oldUsername);
            statement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            throw throwables;
        }
    }

    @Override
    public void delete(Client client) throws SQLException {
        try (Connection connection = Database.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SQL_DELETE_BY_ID);
            statement.setLong(1, client.getId());
            statement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            throw throwables;
        }

    }
}
