package com.chuvashev.dao.impl;

import com.chuvashev.Database;
import com.chuvashev.dao.TrackDAO;
import com.chuvashev.entities.Track;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class TrackDAOImpl implements TrackDAO {

    @Override
    public void save(Track track) throws SQLException {
        try (Connection connection = Database.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SQL_INSERT);
            statement.setString(1, track.getAuthor());
            statement.setString(2, track.getName());
            statement.setDouble(3, track.getSize());
            statement.setLong(4, track.getDuration());
            statement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            throw throwables;
        }
    }

    @Override
    public void delete(Track track) throws SQLException {
        try (Connection connection = Database.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SQL_DELETE);
            statement.setString(1, track.getName());
            statement.setString(2, track.getAuthor());
            statement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            throw throwables;
        }
    }
}
