package com.chuvashev.dao.impl;

import com.chuvashev.Database;
import com.chuvashev.dao.TracklistItemDAO;
import com.chuvashev.entities.Tracklist;
import com.chuvashev.entities.TracklistItem;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

public class TracklistItemDAOImpl implements TracklistItemDAO {
    @Override
    public Optional<TracklistItem> get(long tracklistId) throws SQLException {
        try (Connection connection = Database.getConnection()) {
            TracklistItem tracklistItem = null;
            PreparedStatement statement = connection.prepareStatement(SQL_SELECT_BY_ID);
            statement.setLong(1, tracklistId);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                tracklistItem = new TracklistItem();
                tracklistItem.setId(resultSet.getLong(TracklistItem.ID_COLUMN));
                tracklistItem.setName(resultSet.getString(TracklistItem.NAME_COLUMN));
                tracklistItem.setAuthor(resultSet.getString(TracklistItem.AUTHOR_COLUMN));
                tracklistItem.setTracklistId(tracklistId);
                return Optional.of(tracklistItem);
            }
            return Optional.empty();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            throw throwables;
        }
    }

    @Override
    public void save(TracklistItem tracklistItem) throws SQLException {
        try (Connection connection = Database.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SQL_INSERT);
            statement.setString(1, tracklistItem.getName());
            statement.setString(2, tracklistItem.getAuthor());
            statement.setLong(3, tracklistItem.getTracklistId());
            statement.execute();

            ResultSet keys = statement.getGeneratedKeys();
            if (keys.next()) {
                tracklistItem.setId(keys.getLong(TracklistItem.ID_COLUMN));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            throw throwables;
        }
    }

    @Override
    public void delete(long id) throws SQLException {
        try (Connection connection = Database.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SQL_DELETE_BY_ID);
            statement.setLong(1, id);
            statement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            throw throwables;
        }

    }
}
