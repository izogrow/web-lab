package com.chuvashev.dao;

import com.chuvashev.entities.Tracklist;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public interface TracklistDAO {
    String SQL_INSERT = "insert into " + Tracklist.TABLE_NAME + "( " + Tracklist.TITLE_COLUMN + ", " +
            Tracklist.CLIENT_ID_COLUMN + ") values (?, ?)";
    String SQL_DELETE_BY_ID = "delete from " + Tracklist.TABLE_NAME + " where " + Tracklist.ID_COLUMN + " = ?";
    String SQL_SELECT_ALL = "select " + Tracklist.TITLE_COLUMN + ", " + Tracklist.ID_COLUMN + ", " +
            Tracklist.CLIENT_ID_COLUMN + " from " + Tracklist.TABLE_NAME;
    String SQL_SELECT_BY_CLIENT_ID = "select " + Tracklist.TITLE_COLUMN + ", " + Tracklist.ID_COLUMN + ", " +
            Tracklist.CLIENT_ID_COLUMN + " from " + Tracklist.TABLE_NAME + " where " + Tracklist.CLIENT_ID_COLUMN + " = ?";
    String SQL_SELECT_BY_ID = "select " + Tracklist.TITLE_COLUMN + ", " + Tracklist.ID_COLUMN + ", " +
            Tracklist.CLIENT_ID_COLUMN + " from " + Tracklist.TABLE_NAME + " where " + Tracklist.ID_COLUMN + " = ?";


    Optional<Tracklist> get(long id) throws SQLException;
    List<Tracklist> getAll(long clientId) throws SQLException;
    List<Tracklist> getAll() throws SQLException;
    void save(Tracklist tracklist) throws SQLException;
    //void update(long id, String newTitle) throws SQLException;
    void delete(long id) throws SQLException;
}
