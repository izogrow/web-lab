package com.chuvashev.entities;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Client {
    public static final String TABLE_NAME = "Client";
    public static final String ID_COLUMN = "client_id";
    public static final String NAME_COLUMN = "name";
    public static final String USERNAME_COLUMN = "username";
    public static final String PASSWORD_COLUMN = "password";

    private Long id;
    private String name;
    private String username;
    private String password;
    private List<Tracklist> tracklists = new ArrayList<>();

    public Client() {

    }

    public Client(Long id, String name, String username, String password) {
        this.id = id;
        this.name = name;
        this.username = username;
        this.password = password;
    }

    public static void main(String[] args) {
        Client client = new Client();
        client.addTracklist(new Tracklist((long)4));
        System.out.println(client.getTracklists().toString());
        client.deleteTracklist((long)4);
        System.out.println(client.getTracklists().toString());
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public List<Tracklist> getTracklists() {
        return tracklists;
    }

    public Tracklist findTracklistById(Long id) {
        for (Tracklist tracklist : getTracklists()) {
            if (tracklist.getId().equals(id)) {
                return tracklist;
            }
        }
        return null;
    }

    public void addTracklist(Tracklist tracklist) {
        tracklists.add(tracklist);
    }

    public Track getTrack(String name) {
        for (Tracklist tracklist : getTracklists()) {
            Track track = tracklist.findTrackByName(name);
            if (Optional.ofNullable(track).isPresent())
                return track;
        }
        return null;
    }

    public void deleteTracklist(Long id) {
        for (Tracklist tracklist : getTracklists()) {
            if (tracklist.getId().equals(id)) {
                getTracklists().remove(tracklist);
                return;
            }
        }
    }

    public int size() {
        return getTracklists().size();
    }

    @Override
    public String toString() {
        return "Client{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", tracklists=" + tracklists +
                '}';
    }
}
