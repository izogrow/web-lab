package com.chuvashev.entities;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.Objects;


public class Track implements Serializable {
    public static final String TABLE_NAME = "Track";
    public static final String NAME_COLUMN = "name";
    public static final String AUTHOR_COLUMN = "author";
    public static final String SIZE_COLUMN = "size";
    public static final String DURATION_COLUMN = "duration";
    public static final String DESCRIPTION_COLUMN = "description";

    private String name;
    private String author;
    private Double size;
    private Long duration;
    private String description;

    @JsonCreator
    public Track(@JsonProperty("name") String name, @JsonProperty("author") String author, @JsonProperty("size") Double size,
                 @JsonProperty("duration") Long duration, @JsonProperty("description") String description) {
        this.name = name;
        this.author = author;
        this.description = description;

        if (size > 0)           // Проверяем корректность значений по заданию
            this.size = size;
        if (duration > 0)
            this.duration = duration;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Double getSize() {
        return size;
    }
    public void setSize(Double size) {
        this.size = size;
    }
    public Long getDuration() {
        return duration;
    }
    public void setDuration(Long duration) {
        this.duration = duration;
    }
    public String getAuthor() {
        return author;
    }
    public void setAuthor(String author) {
        this.author = author;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public static void writeToJson(Track track) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.writeValue(new File(track.name + ".json"), track);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Track readFromJson(String path) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readValue(new File(path), Track.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String toString() {
        return "Track{" +
                "name='" + name + '\'' +
                ", author=" + author +
                ", size=" + size +
                ", duration=" + duration +
                ", description=" + description +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Track track = (Track) o;
        return Objects.equals(name, track.name) &&
                Objects.equals(size, track.size) &&
                Objects.equals(duration, track.duration) &&
                Objects.equals(author, track.author);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, author, size, duration);
    }
}
