package com.chuvashev.entities;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.util.*;

public class Tracklist implements Serializable {
    public static final String TABLE_NAME = "Tracklist";
    public static final String ID_COLUMN = "tracklist_id";
    public static final String TITLE_COLUMN = "title";
    public static final String CLIENT_ID_COLUMN = "client_id";

    private Long id;
    private String title;
    private Long clientId;
    @JsonProperty
    private List<Track> tracks = new LinkedList<>();

    public static void main(String[] args) {


        Tracklist tracklist = new Tracklist((long) 2);
        tracklist.readTracksFromJson("2_tracklist.json");
        tracklist.addTrack(new Track("aiobahn", "if we never", 2.4, (long)22000, ""));
        tracklist.writeToJson();
        System.out.println(tracklist.toString());

    }

    public Tracklist() {

    }

    public Tracklist(Long id) {
        this.id = id;
        tracks = new ArrayList<Track>();
    }

    @JsonCreator
    public Tracklist(@JsonProperty("tracklist_id") Long id, @JsonProperty("tracks") List<Track> tracks,
                     @JsonProperty("client_id") Long client_id) {
        this.id = id;
        this.tracks = tracks;
        this.clientId = client_id;
    }

    public Long getId() {
        return id;
    }
    public List<Track> getTracks() {
        return tracks;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public Long getClientId() {
        return clientId;
    }

    public int length() {
        return tracks.size();
    }

    public void addTrack(Track track) {
        tracks.add(track);
    }

    public void deleteTrack(int index) {
        tracks.remove(index);
    }

    public Track findTrackByName(String name) {
        for (Track track : getTracks()) {
            if (track.getName().equals(name)) {
                return track;
            }
        }
        return null;

    }

    public void readTracksFromJson(String path) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            Tracklist tracklist = objectMapper.readValue(new File(path), Tracklist.class);
            for (Track track: tracklist.getTracks()) {
                addTrack(track);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
       // return null; // TODO: выбрасывать исключение
    }

    public void writeToJson() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            objectMapper.writeValue(new File(getId() + "_tracklist.json"), this);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeToJson(String path) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            objectMapper.writeValue(new File(path), this);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sort() {
        Set<Track> TracksWithoutDuplicatesSet = new HashSet<>(tracks);
        tracks.clear();
        tracks.addAll(TracksWithoutDuplicatesSet);

        tracks.sort(new Comparator<Track>() {
            @Override
            public int compare(Track o1, Track o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
    }

    @Override
    public String toString() {
        return "Tracklist{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", clientId=" + clientId +
                ", tracks=" + tracks +
                '}';
    }
}
