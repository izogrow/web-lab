package com.chuvashev.entities;

public class TracklistItem {
    public static final String TABLE_NAME = "Tracklist_item";
    public static final String ID_COLUMN = "id";
    public static final String TRACKLIST_ID_COLUMN = "tracklist_id";
    public static final String NAME_COLUMN = "name";
    public static final String AUTHOR_COLUMN = "author";

    private Long id;
    private Long tracklistId;
    private String name;
    private String author;

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public Long getTracklistId() {
        return tracklistId;
    }
    public void setTracklistId(Long tracklistId) {
        this.tracklistId = tracklistId;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getAuthor() {
        return author;
    }
    public void setAuthor(String author) {
        this.author = author;
    }

    @Override
    public String toString() {
        return "TracklistItem{" +
                "id=" + id +
                ", tracklistId=" + tracklistId +
                ", name='" + name + '\'' +
                ", author='" + author + '\'' +
                '}';
    }
}
