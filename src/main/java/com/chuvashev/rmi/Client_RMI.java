package com.chuvashev.rmi;

import com.chuvashev.entities.Tracklist;
import com.chuvashev.rmi.services.SortService;

import java.io.FileWriter;
import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Client_RMI {
    public static void main(String[] args)  {
        Tracklist tracklist = new Tracklist((long)2);
        try {
            Registry registry = LocateRegistry.getRegistry();
            SortService server = (SortService) registry.lookup("MessengerService");


            tracklist.readTracksFromJson("2_tracklist.json");

            Tracklist sortedTracklist = server.sortTracklist(tracklist);

            System.out.println(tracklist);
            System.out.println(sortedTracklist);

            sortedTracklist.writeToJson("result.json");
        } catch (RemoteException | NotBoundException e) {
            try {
                FileWriter fileWriter = new FileWriter("error.txt");
                fileWriter.write(e.toString());
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            e.printStackTrace();
        }
    }
}