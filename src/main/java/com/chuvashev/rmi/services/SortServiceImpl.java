package com.chuvashev.rmi.services;

import com.chuvashev.entities.Tracklist;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;


public class SortServiceImpl implements SortService {     //  Remote Object

    public static void main(String[] args) {
        SortServiceImpl messengerService = new SortServiceImpl();
        messengerService.createStubAndBind();


    }

    @Override
    public String sendMessage(String clientMessage) {
        return "Client Message".equals(clientMessage) ? "Server message" : null;
    }

    @Override
    public Tracklist sortTracklist(Tracklist clientTracklist) throws RemoteException {
        clientTracklist.sort();
        return clientTracklist;
    }

    // И здесь будут другие методы класса, но невидимые для клиента.

    public void createStubAndBind() {
        SortService stub = null;
        try {
            stub = (SortService) UnicastRemoteObject.exportObject((SortService) this, 0);
            Registry registry = LocateRegistry.createRegistry(1099);
            registry.rebind("MessengerService", stub);
            System.out.println("Server ready!");
        } catch (RemoteException e) {
            System.err.println("Server exception: " + e.toString());
            e.printStackTrace();
        }

    }
}