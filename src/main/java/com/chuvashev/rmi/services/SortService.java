package com.chuvashev.rmi.services;

import com.chuvashev.entities.Tracklist;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface SortService extends Remote {      // только эти методы доступны удалено
    String sendMessage(String clientMessage) throws RemoteException;
    Tracklist sortTracklist(Tracklist clientTracklist) throws RemoteException;
}
