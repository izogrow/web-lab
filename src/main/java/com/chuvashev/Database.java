package com.chuvashev;

import java.sql.*;

public class Database {
    private final static String url = "jdbc:postgresql://localhost/musicloud";   // какое название у название дб!?
    private final static String user = "postgres";
    private final static String password = "123";     // пароль?

    public static Connection getConnection(){
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url, user, password);
            System.out.println("Connected to the PostgreSQL server successfully.");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return conn;
    }

    public static void main(String[] args) {
        Database database = new Database();
    //    database.connect();
    }
}
